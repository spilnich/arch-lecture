package group.spd.arch.task;

import java.util.List;

public interface TaskRepository {

	Task save(Task task);

	List<Task> getAll();
}
