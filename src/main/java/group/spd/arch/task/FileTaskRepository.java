package group.spd.arch.task;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

public class FileTaskRepository implements TaskRepository {

    private final ObjectMapper om;
    private final File db;

    public FileTaskRepository(ObjectMapper om) {
        this.om = om;
        db = new File("/tmp/db/task.json");
        if (!db.exists()) {
            try {
                db.getParentFile().mkdirs();
                db.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Task save(Task task) {
        final List<Task> tasks = new ArrayList<>(getAll());
        task.setId(tasks.size() + 1);
        tasks.add(task);
        try {
            om.writeValue(db, tasks);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return task;
    }

    @Override
    public List<Task> getAll() {
        final TypeReference<List<Task>> reference = new TypeReference<>() {
        };
        try {
            return om.readValue(db, reference);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return emptyList();
        }
    }
}
