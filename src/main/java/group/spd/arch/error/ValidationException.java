package group.spd.arch.error;

public class ValidationException extends RuntimeException {

	public ValidationException(String message) {
		super(message);
	}
}
