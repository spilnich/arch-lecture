package group.spd.arch.person;

import java.util.Optional;

public interface PersonProvider {

	Optional<Person> getById(int id);
}
