package group.spd.arch.notification;

public interface NotificationService {

    public void onTaskCreated(TaskCreatedEvent event);
}
