package group.spd.arch.notification;

import java.time.LocalDate;

public class TaskCreatedEvent {

    private final String taskDescription;
    private final LocalDate deadline;
    private final String assigneeEmail;

    public TaskCreatedEvent(String taskDescription, LocalDate deadline, String assigneeEmail) {
        this.taskDescription = taskDescription;
        this.deadline = deadline;
        this.assigneeEmail = assigneeEmail;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public String getAssigneeEmail() {
        return assigneeEmail;
    }
}
