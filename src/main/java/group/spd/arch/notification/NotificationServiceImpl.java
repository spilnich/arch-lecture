package group.spd.arch.notification;

import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService{

    @Override
    public void onTaskCreated(TaskCreatedEvent event) {
        System.out.println("Send notification to email " + event.getAssigneeEmail());
        System.out.println("==================================================");
        System.out.println("New task was assigned to you.");
        System.out.println("Description: " + event.getTaskDescription());
        System.out.println("Deadline: " + event.getDeadline());
        System.out.println("==================================================");
    }
}
