package group.spd.arch;

import group.spd.arch.error.ConflictException;
import group.spd.arch.error.ValidationException;
import group.spd.arch.person.Person;
import group.spd.arch.person.PersonController;
import group.spd.arch.task.Task;
import group.spd.arch.task.TaskController;

import java.time.LocalDate;
import java.util.Scanner;

public class AppDispatcher {

    private final PersonController personController;
    private final TaskController taskController;
    private final Scanner scanner;

    public AppDispatcher(PersonController personController, TaskController taskController) {
        this.personController = personController;
        this.taskController = taskController;
        scanner = new Scanner(System.in);
        scanner.useDelimiter("\n");
    }

    public void run() {
        while (true) {
            printMainMenu();
            int selection = scanner.nextInt();
            switch (selection) {
                case 1:
                    printPersons();
                    break;
                case 2:
                    createPerson();
                    break;
                case 3:
                    printTasks();
                    break;
                case 4:
                    createTask();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Invalid option");
                    break;
            }
        }
    }

    private void printMainMenu() {
        System.out.println();
        System.out.println("Main menu:");
        System.out.println("1. Print all persons");
        System.out.println("2. Add new Person");
        System.out.println("3. Print all tasks");
        System.out.println("4. Add new task");
        System.out.println("5. Quit");
    }

    private void createPerson() {
        final Person person = new Person();

        System.out.print("Enter name: ");
        person.setName(scanner.next());

        System.out.print("Enter email: ");
        person.setEmail(scanner.next());

        try {
            personController.createPerson(person);
        } catch (ValidationException e) {
            System.out.println(e.getMessage() + " Please, try again");
            createPerson();
        }
    }

    private void createTask() {
        final Task task = new Task();

        System.out.print("Enter task description: ");
        task.setDescription(scanner.next());

        System.out.println("Select assignee for the task:");
        printPersons();
        task.setAssigneeId(scanner.nextInt());

        System.out.println("Enter due date: ");
        final LocalDate dueDate = LocalDate.parse(scanner.next());
        task.setDeadline(dueDate);

        try {
            taskController.createTask(task);
        } catch (ValidationException | ConflictException e) {
            System.out.println(e.getMessage() + " Please, try again");
            createTask();
        }
    }

    private void printPersons() {
        personController.getAll()
                .forEach(person -> System.out.printf("%d. Name: %s, email: %s%n", person.getId(), person.getName(), person.getEmail()));
    }

    private void printTasks() {
        taskController.getAll()
                .forEach(task -> System.out.printf("%d. Description: %s, due date: %s%n", task.getId(), task.getDescription(), task.getDeadline()));
    }
}
