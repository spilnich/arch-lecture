package group.spd.arch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import group.spd.arch.notification.NotificationService;
import group.spd.arch.notification.NotificationServiceImpl;
import group.spd.arch.person.DbPersonRepository;
import group.spd.arch.person.FilePersonRepository;
import group.spd.arch.person.PersonController;
import group.spd.arch.person.PersonService;
import group.spd.arch.task.DbTaskRepository;
import group.spd.arch.task.FileTaskRepository;
import group.spd.arch.task.TaskController;
import group.spd.arch.task.TaskRepository;
import group.spd.arch.task.TaskService;

import javax.sql.DataSource;

public class App {

    public static void main(String[] args) {
        final ObjectMapper om = buildObjectMapper();
        final DataSource dataSource = buildDataSource();

        final DbPersonRepository personRepository = new DbPersonRepository(dataSource);
        final TaskRepository taskRepository = new DbTaskRepository(dataSource);

        final NotificationService notificationService = new NotificationServiceImpl();
        final PersonService personService = new PersonService(personRepository);
        final TaskService taskService = new TaskService(taskRepository, personRepository, notificationService);

        final PersonController personController = new PersonController(personService);
        final TaskController taskController = new TaskController(taskService);

        new AppDispatcher(personController, taskController).run();
    }

    private static ObjectMapper buildObjectMapper() {
        final ObjectMapper om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return om;
    }

    private static DataSource buildDataSource() {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/arch");
        config.setUsername("arch-user");
        config.setPassword("arch-pass");
        config.setMaximumPoolSize(20);

        return new HikariDataSource(config);
    }
}
